.PHONY: test compile install

PREFIX ?= /usr/local

test:
	go test -count 1 ./...

compile:
	GOBIN="$(CURDIR)/bin" go install ./cmd/ai

install: compile
	mkdir -p $(DESTDIR)$(PREFIX)/bin/
	install -m755 bin/ai $(DESTDIR)$(PREFIX)/bin/ai
