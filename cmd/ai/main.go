package main

import (
	"fmt"
	"os"

	"gitlab.com/pbajao/ai-client/internal/api"
	"gitlab.com/pbajao/ai-client/internal/config"
	"gitlab.com/pbajao/ai-client/internal/cmd"
)

func main() {
	config, err := config.LoadConfig()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	apiClient := &api.Client{
		Host: config.Host,
		Token: config.Token,
	}

	rootCmd := cmd.NewRootCmd(apiClient)

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
