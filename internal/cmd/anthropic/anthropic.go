package anthropic

import (
	"github.com/spf13/cobra"
	"gitlab.com/pbajao/ai-client/internal/api"
)

func NewAnthropicCmd(apiClient api.ClientInterface) *cobra.Command {
	anthropicCmd := &cobra.Command{
		Use:   "anthropic",
		Short: "Command for interacting with Anthropic API",
	}

	anthropicCmd.AddCommand(NewCompleteCmd(apiClient))

	return anthropicCmd
}
