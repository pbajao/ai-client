package anthropic

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pbajao/ai-client/internal/mockapi"
	"gitlab.com/pbajao/ai-client/internal/testhelper"
)

func TestAnthropicExecute(t *testing.T) {
	out, _ := testhelper.ExecuteCommand(NewAnthropicCmd(&mockapi.Client{}), []string{})

	require.Contains(t, out, "Command for interacting with Anthropic API")
}
