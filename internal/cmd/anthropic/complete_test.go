package anthropic

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pbajao/ai-client/internal/api"
	"gitlab.com/pbajao/ai-client/internal/mockapi"
	"gitlab.com/pbajao/ai-client/internal/testhelper"
)

func TestCompleteExecuteSuccess(t *testing.T) {
	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewCompleteCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		completeParams := params.(*api.AnthropicCompleteParams)

		require.Equal(t, "This is prompt.", completeParams.Prompt)
		require.Equal(t, "claude-instant-1", completeParams.Model)
		require.Equal(t, 1024, completeParams.MaxTokensToSample)

		return "OK", nil
	}

	out, _ := testhelper.ExecuteCommand(
		cmd,
		[]string{"--prompt", "This is prompt.", "--model", "claude-instant-1", "--max-tokens", "1024"},
	)

	require.Equal(t, "OK", out)
}

func TestCompleteExecuteSuccess_PromptFrom(t *testing.T) {
	tmpFile, err := os.CreateTemp("", "prompt.txt")
	require.NoError(t, err)
	defer os.Remove(tmpFile.Name())

	_, err = tmpFile.WriteString("This is prompt from file.")
	require.NoError(t, err)

	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewCompleteCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		completeParams := params.(*api.AnthropicCompleteParams)

		require.Equal(t, "This is prompt from file.", completeParams.Prompt)

		return "OK", nil
	}

	out, _ := testhelper.ExecuteCommand(
		cmd,
		[]string{"--prompt-from", tmpFile.Name()},
	)

	require.Equal(t, "OK", out)
}

func TestCompleteExecuteFailure(t *testing.T) {
	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewCompleteCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		return "", fmt.Errorf("This is an error")
	}

	_, errOut := testhelper.ExecuteCommand(
		cmd,
		[]string{"--prompt", "This is prompt."},
	)

	require.Equal(t, "Error: anthropic complete: This is an error\n", errOut)
}
