package anthropic

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/pbajao/ai-client/internal/api"
)

func NewCompleteCmd(apiClient api.ClientInterface) *cobra.Command {
	var promptFrom string
	params := &api.AnthropicCompleteParams{}

	completeCmd := &cobra.Command{
		Use:   "complete",
		Short: "Command for interacting with Anthropic Complete API endpoint",
		RunE: func(cmd *cobra.Command, args []string) error {
			if promptFrom != "" && params.Prompt == "" {
				params.Prompt = readPromptFromFile(promptFrom)
			}

			res, err := apiClient.AnthropicComplete(params)
			if err != nil {
				return fmt.Errorf("anthropic complete: %w", err)
			}

			fmt.Fprintf(cmd.OutOrStdout(), res)

			return nil
		},
	}

	completeCmd.Flags().StringVarP(&params.Prompt, "prompt", "c", "", "Prompt that will be sent to model")
	completeCmd.Flags().StringVarP(&params.Model, "model", "m", "claude-2", "Model to be used")
	completeCmd.Flags().IntVarP(&params.MaxTokensToSample, "max-tokens", "", 2048, "Max tokens to sample")
	completeCmd.Flags().StringVarP(&promptFrom, "prompt-from", "", "", "Read prompt from file that will be sent to model")

	return completeCmd
}

func readPromptFromFile(path string) string {
	prompt, err := os.ReadFile(path)
	if err != nil {
		return ""
	}

	return string(prompt)
}
