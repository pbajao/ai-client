package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/pbajao/ai-client/internal/api"
	"gitlab.com/pbajao/ai-client/internal/cmd/anthropic"
	"gitlab.com/pbajao/ai-client/internal/cmd/vertex"
)

func NewRootCmd(apiClient api.ClientInterface) *cobra.Command {
	rootCmd := &cobra.Command{
		Use:   "ai",
		Short: "A CLI tool used for interacting with GitLab experimentation API for proxying requests to Vertex AI and Anthropic",
	}

	rootCmd.AddCommand(anthropic.NewAnthropicCmd(apiClient))
	rootCmd.AddCommand(vertex.NewVertexCmd(apiClient))

	return rootCmd
}
