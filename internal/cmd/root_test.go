package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pbajao/ai-client/internal/mockapi"
	"gitlab.com/pbajao/ai-client/internal/testhelper"
)

func TestRootExecute(t *testing.T) {
	out, _ := testhelper.ExecuteCommand(NewRootCmd(&mockapi.Client{}), []string{})

	require.Contains(t, out, "A CLI tool used for interacting with GitLab experimentation API for proxying requests to Vertex AI and Anthropic")
}
