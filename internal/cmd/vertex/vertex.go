package vertex

import (
	"github.com/spf13/cobra"
	"gitlab.com/pbajao/ai-client/internal/api"
)

func NewVertexCmd(apiClient api.ClientInterface) *cobra.Command {
	vertexCmd := &cobra.Command{
		Use:   "vertex",
		Short: "Command for interacting with Vertex AI API",
	}

	vertexCmd.AddCommand(NewChatCmd(apiClient))

	return vertexCmd
}
