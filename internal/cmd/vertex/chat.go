package vertex

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/pbajao/ai-client/internal/api"
)

func NewChatCmd(apiClient api.ClientInterface) *cobra.Command {
	var contentFrom string
	params := &api.VertexChatParams{}

	chatCmd := &cobra.Command{
		Use:   "chat",
		Short: "Command for interacting with Vertex AI Chat API endpoint",
		RunE: func(cmd *cobra.Command, args []string) error {
			if contentFrom != "" && params.Content == "" {
				params.Content = readContentFromFile(contentFrom)
			}

			res, err := apiClient.VertexChat(params)
			if err != nil {
				return fmt.Errorf("vertex chat: %w", err)
			}

			fmt.Fprintf(cmd.OutOrStdout(), res)

			return nil
		},
	}

	chatCmd.Flags().StringVarP(&params.Content, "content", "c", "", "Content that will be sent to model")
	chatCmd.Flags().StringVarP(&contentFrom, "content-from", "", "", "Read content from file that will be sent to model")

	return chatCmd
}

func readContentFromFile(path string) string {
	content, err := os.ReadFile(path)
	if err != nil {
		return ""
	}

	return string(content)
}
