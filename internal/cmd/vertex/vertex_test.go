package vertex

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pbajao/ai-client/internal/mockapi"
	"gitlab.com/pbajao/ai-client/internal/testhelper"
)

func TestVertexExecute(t *testing.T) {
	out, _ := testhelper.ExecuteCommand(NewVertexCmd(&mockapi.Client{}), []string{})

	require.Contains(t, out, "Command for interacting with Vertex AI API")
}
