package vertex

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pbajao/ai-client/internal/api"
	"gitlab.com/pbajao/ai-client/internal/mockapi"
	"gitlab.com/pbajao/ai-client/internal/testhelper"
)

func TestChatExecuteSuccess(t *testing.T) {
	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewChatCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		chatParams := params.(*api.VertexChatParams)

		require.Equal(t, "This is content.", chatParams.Content)

		return "OK", nil
	}

	out, _ := testhelper.ExecuteCommand(
		cmd,
		[]string{"--content", "This is content."},
	)

	require.Equal(t, "OK", out)
}

func TestChatExecuteSuccess_ContentFrom(t *testing.T) {
	tmpFile, err := os.CreateTemp("", "content.txt")
	require.NoError(t, err)
	defer os.Remove(tmpFile.Name())

	_, err = tmpFile.WriteString("This is content from file.")
	require.NoError(t, err)

	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewChatCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		chatParams := params.(*api.VertexChatParams)

		require.Equal(t, "This is content from file.", chatParams.Content)

		return "OK", nil
	}

	out, _ := testhelper.ExecuteCommand(
		cmd,
		[]string{"--content-from", tmpFile.Name()},
	)

	require.Equal(t, "OK", out)
}

func TestChatExecuteFailure(t *testing.T) {
	mockClient := &mockapi.Client{Host: "https://example.com", Token: "pat"}
	cmd := NewChatCmd(mockClient)

	mockClient.Handler = func(params api.EndpointParams) (string, error) {
		return "", fmt.Errorf("This is an error")
	}

	_, errOut := testhelper.ExecuteCommand(
		cmd,
		[]string{"--content", "This is content."},
	)

	require.Equal(t, "Error: vertex chat: This is an error\n", errOut)
}
