package api

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAnthropicCompleteSuccess(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		body, err := io.ReadAll(req.Body)
		require.NoError(t, err)

		expected := `{"prompt":"This is a prompt.","model":"model","max_tokens_to_sample":1024}`
		require.Equal(t, expected , string(body))
		rw.Write([]byte("OK"))
	}))

	defer server.Close()

	client := &Client{Host: server.URL, Token: "pat"}
	res, err := client.AnthropicComplete(&AnthropicCompleteParams{
		Prompt: "This is a prompt.",
		Model: "model",
		MaxTokensToSample: 1024,
	})

	require.NoError(t, err)
	require.Equal(t, "OK", res)
}
