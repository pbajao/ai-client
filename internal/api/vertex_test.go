package api

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVertexChatSuccess(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		body, err := io.ReadAll(req.Body)
		require.NoError(t, err)
		require.Equal(t, `{"content":"This is a content."}`, string(body))
		rw.Write([]byte("OK"))
	}))

	defer server.Close()

	client := &Client{Host: server.URL, Token: "pat"}
	res, err := client.VertexChat(&VertexChatParams{Content: "This is a content."})

	require.NoError(t, err)
	require.Equal(t, "OK", res)
}
