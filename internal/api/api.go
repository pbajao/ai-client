package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const (
	ApiVersion = "/api/v4"
)

type ClientInterface interface {
	VertexChat(*VertexChatParams) (string, error)
	AnthropicComplete(*AnthropicCompleteParams) (string, error)
}

type Client struct {
	Host string
	Token string
}

type EndpointParams interface {}

func (c *Client) makeRequest(endpoint string, params EndpointParams) ([]byte, error) {
	jsonBody, err := serializeParams(params)
	if err != nil {
		return nil, fmt.Errorf("makeRequest: could not serialize params: %w", err)
	}

	bodyReader := bytes.NewReader(jsonBody)

	url := buildUrl(c.Host, endpoint)

	req, err := http.NewRequest(http.MethodPost, url, bodyReader)
	if err != nil {
		return nil, fmt.Errorf("makeRequest: could not create request: %w", err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Private-Token", c.Token)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("makeRequest: error making http request: %w", err)
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("makeRequest: could not read response body: %w", err)
	}

	return resBody, nil
}

func serializeParams(params EndpointParams) ([]byte, error) {
	b, err := json.Marshal(params)
	if err != nil {
		return nil, fmt.Errorf("serializeParams: failed to marshal struct to JSON: %w", err)
	}

	return b, nil
}

func buildUrl(host string, endpoint string) string {
	return host + ApiVersion + endpoint
}
