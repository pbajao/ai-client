package api

import (
	"fmt"
)

const (
	VertexChatEndpoint = "/ai/experimentation/vertex/chat"
)

type VertexChatParams struct {
	Content string `json:"content"`
}

func (c *Client) VertexChat(params *VertexChatParams) (string, error) {
	res, err := c.makeRequest(VertexChatEndpoint, params)
	if err != nil {
		return "", fmt.Errorf("VertexChat: request failed: %w", err)
	}

	return string(res), nil
}
