package api

import (
	"fmt"
)

const (
	AnthropicCompleteEndpoint = "/ai/experimentation/anthropic/complete"
)

type AnthropicCompleteParams struct {
	Prompt string `json:"prompt"`
	Model string `json:"model"`
	MaxTokensToSample int `json:"max_tokens_to_sample"`
}

func (c *Client) AnthropicComplete(params *AnthropicCompleteParams) (string, error) {
	res, err := c.makeRequest(AnthropicCompleteEndpoint, params)
	if err != nil {
		return "", fmt.Errorf("AnthropicComplete: request failed: %w", err)
	}

	return string(res), nil
}
