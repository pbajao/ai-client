package config

import (
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func TestLoadConfigDefault(t *testing.T) {
	config, err := LoadConfig()
	defer config.Reset()

	require.NoError(t, err)
	require.Equal(t, "https://gitlab.com", config.Host)
	require.Equal(t, "", config.Token)
}

func TestLoadConfigEnv(t *testing.T) {
	t.Setenv("GITLAB_HOST", "https://example.com")
	t.Setenv("GITLAB_TOKEN", "thisisatoken")

	config, err := LoadConfig()
	defer config.Reset()

	require.NoError(t, err)
	require.Equal(t, "https://example.com", config.Host)
	require.Equal(t, "thisisatoken", config.Token)
}

func TestLoadConfigFile(t *testing.T) {
	viper.AddConfigPath("testdata")

	config, err := LoadConfig()
	defer config.Reset()

	require.NoError(t, err)
	require.Equal(t, "https://example.com", config.Host)
	require.Equal(t, "thisisatoken", config.Token)
}
