package config

import (
	"github.com/spf13/viper"
)

const (
	DefaultHost = "https://gitlab.com"
)

type config struct {
	Host string
	Token string
}

func init() {
	setupViper()
}

func LoadConfig() (*config, error) {
	err := viper.ReadInConfig()
	if err != nil {
		// Only return err if it's not a viper.ConfigFileNotFoundError since
		// we want to continue if config file doesn't exist.
		if err, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	host := viper.GetString("host")
	token := viper.GetString("token")

	return &config{Host: host, Token: token}, nil
}

func (c *config) Reset() {
	viper.Reset()
	setupViper()
}

func setupViper() {
	viper.SetDefault("host", DefaultHost)
	viper.SetEnvPrefix("GITLAB")
	viper.BindEnv("HOST")
	viper.BindEnv("TOKEN")
	viper.SetConfigType("toml")
	viper.SetConfigName("config")
	viper.AddConfigPath("$HOME/.ai")
	viper.AddConfigPath(".")
}
