package mockapi

import (
	"gitlab.com/pbajao/ai-client/internal/api"
)

type Client struct {
	Host string
	Token string
	Handler func(params api.EndpointParams) (string, error)
}
