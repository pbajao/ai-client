package mockapi

import (
	"gitlab.com/pbajao/ai-client/internal/api"
)

func (c *Client) VertexChat(params *api.VertexChatParams) (string, error) {
	res, err := c.Handler(params)

	return res, err
}
