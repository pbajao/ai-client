package mockapi

import (
	"gitlab.com/pbajao/ai-client/internal/api"
)

func (c *Client) AnthropicComplete(params *api.AnthropicCompleteParams) (string, error) {
	res, err := c.Handler(params)

	return res, err
}
