package testhelper

import (
	"bytes"

	"github.com/spf13/cobra"
)

func ExecuteCommand(cmd *cobra.Command, args []string) (string, string) {
	out := &bytes.Buffer{}
	errOut := &bytes.Buffer{}

	cmd.SetOut(out)
	cmd.SetErr(errOut)
	cmd.SetArgs(args)
	cmd.Execute()

	return out.String(), errOut.String()
}
